### VideoCapture--视频的获取操作  

  VideoCapture既支持从视频文件（.avi,mpg格式）读取，也支持从摄像机中读取。想要获取视频需要先创建一个VideoCapture对象，VideoCapture对象的创建方式有以下三种:  
  方式一：是从文件（.MPG或.AVI格式）中读取视频，对象创建以后，OpenCV将会打开文件并做好准备读取它，如果打开成功，我们将可以开始读取视频的帧，并且cv::VideoCapture的成员函数isOpened()将会返回true（建议在打开视频或摄像头时都使用该成员函数判断是否打开成功）。  
```
方法：  cv::VideoCapture capture(const string& filename);  // 从视频文件读取 
```
```
例程：  cv::VideoCapture capture("C:/Users/DADA/DATA/gogo.avi");  // 从视频文件读取 
```
  
  方式二：是从摄像机中读取视频，这种情况下，我们会给出一个标识符，用于表示我们想要访问的摄像机，及其与操作系统的握手方式。对于摄像机而言，这个标志符就是一个标志数字——如果只有1个摄像机，那么就是0，如果系统中有多个摄像机，那么只要将其向上增加即可。标识符另外一部分是摄像机域（camera domain），用于表示摄像机的类型，这个域值可以是下面任一预定义常量。
  
方式三：先创建一个捕获对象，然后通过成员函数open()来设定打开的信息。  

```
VideoCapture.open( "C:/Users/DADA/DATA/gogo.avi" )
```
将视频帧读取到cv::Mat矩阵中，有两种方式：一种是read()操作；另一种是 “>>”操作。  

```
cap.read(frame); //读取方式一  
cap >> frame; //读取方式二 
```  
代码如下：  

```
import cv2
import numpy as np
def video_demo():
    capture = cv2.VideoCapture(0)#0为电脑内置摄像头
    while(True):
        ret, frame = capture.read()#摄像头读取,ret为是否成功打开摄像头,true,false。 frame为视频的每一帧图像
        frame = cv2.flip(frame, 1)#摄像头是和人对立的，将图像左右调换回来正常显示。
        cv2.imshow("video", frame)
        c = cv2.waitKey(50)
        if c == 27:
            break
video_demo()
cv2.destroyAllWindows()
```  
(cv2.waitKey()与cv2.destroyAllWindows()的区别：  
cv2.waitKey()是一个键盘绑定函数。它的时间量度是毫秒ms。函数会等待（n）里面的n毫秒，看是否有键盘输入。若有键盘输入，则返回按键的ASCII值。没有键盘输入，则返回-1.一般设置为0，他将无线等待键盘的输入。  
cv2.destroyAllWindows() 用来删除窗口的，（）里不指定任何参数，则删除所有窗口，删除特定的窗口，往（）输入特定的窗口值。）  
  
ps:先创建窗口，改变窗口大小可以使用cv2.nameWindow(),初始设定函数标签是cv2.WINDOW_AUTOSIZE,将标签改为cv2.WINDOW_NORMAL控制窗口大小。  
可以利用cv2.waitKey()返回的值进行截屏操作。

```
cv2.namedWindow('messi', cv2.WINDOW_AUTOSIZE)
img = cv2.imread('messi4.jpg')
cv2.imshow('messi', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

cv2.namedWindow('messi', cv2.WINDOW_NORMAL)
cv2.imshow('messi', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

```  
截屏操作只需要添加一个语句，当按那个键时截屏:

```
//按q键时截屏
if cv2.waitKey(50) & 0xFF == ord('q)：
    cv2.imwrite("test.png",img)
    break
```
也可以添加循环语句一直截屏并保存



